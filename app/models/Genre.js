module.exports = (sequelize, Sequelize) => {
    const Actor = sequelize.define('genre', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        type: Sequelize.STRING(30)
    })
    return Actor;
}
