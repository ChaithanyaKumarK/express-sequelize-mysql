const express = require('express');
const {
    directors: Director,
    genres: Genre,
    movies: Movie,
    actors: Actor,
} = require('../database/connection')
const logger = require('../../logger');

const path = require('path');
const fs = require('fs');

const router = express.Router();

router.get('/director', (req, res) => {
    fs.readFile(path.join(__dirname, '../movies.json'), 'utf-8', (err, movies) => {
        if (err) {
            logger.error(err);
            res.status(500).json({
                message: "internal server error"
            });
            res.end();
        } else {
            movies = JSON.parse(movies);
            uniqueDirectors = [];
            movies.forEach(movie => {
                let director = movie.Director;
                if (!uniqueDirectors.includes(director)) {
                    uniqueDirectors.push(director);
                }
            })
            uniqueDirectors.forEach(async (uniqueDirector) => {
                let director = {
                    name: uniqueDirector
                }

                await Director
                    .create(director)
                    .catch(err => {
                        logger.error(err);
                        res.status(500).json({
                            message: "internal server error"
                        });
                        res.end();
                    });
            })
            res.status(200).json({
                message: "directors data added successfully",
            })
            res.end();
        }
    })
})


router.get('/actors', (req, res) => {
    fs.readFile(path.join(__dirname, '../movies.json'), 'utf-8', (err, movies) => {
        if (err) {
            logger.error(err);
            res.status(500).json({
                message: "internal server error"
            });
            res.end();
        } else {
            movies = JSON.parse(movies);
            uniqueActors = [];
            movies.forEach(movie => {
                let actor = movie.Actor;
                if (!uniqueActors.includes(actor)) {
                    uniqueActors.push(actor);
                }
            })
            uniqueActors.forEach(async (uniqueActor) => {
                let actor = {
                    name: uniqueActor
                }

                await Actor
                    .create(actor)
                    .catch(err => {
                        logger.error(err);
                        res.status(500).json({
                            message: "internal server error"
                        });
                        res.end();
                    });
            })
            res.status(200).json({
                message: "actors data added successfully",
            })
            res.end();
        }
    })
})


router.get('/genres', (req, res) => {
    fs.readFile(path.join(__dirname, '../movies.json'), 'utf-8', (err, movies) => {
        if (err) {
            logger.error(err);
            res.status(500).json({
                message: "internal server error"
            });
            res.end();
        } else {
            movies = JSON.parse(movies);
            uniqueGenres = [];
            movies.forEach(movie => {
                let genre = movie.Genre;
                if (!uniqueGenres.includes(genre)) {
                    uniqueGenres.push(genre);
                }
            })
            uniqueGenres.forEach(async (uniqueGenre) => {
                let genre = {
                    type: uniqueGenre
                }

                await Genre
                    .create(genre)
                    .catch(err => {
                        logger.error(err);
                        res.status(500).json({
                            message: "internal server error"
                        });
                        res.end();
                    });
            })
            res.status(200).json({
                message: "genres data added successfully",
            })
            res.end();
        }
    })
})



router.get('/movies', (req, res) => {
    fs.readFile(path.join(__dirname, '../movies.json'), 'utf-8', async (err, movies) => {
        if (err) {
            logger.error(err);
            res.status(500).json({
                message: "internal server error"
            });
            res.end();
        } else {
            movies = JSON.parse(movies);
            const databaseDirector = await Director.findAll();
            const databaseActor = await Actor.findAll();
            const databaseGenre = await Genre.findAll();
            let newMovieList = [];

            movies.forEach(movie => {
                let newMovie = {};
                databaseDirector.forEach(director => {
                    if (movie.Director == director.name) {
                        newMovie["directorId"] = director.id;
                    }
                })

                databaseActor.forEach(actor => {
                    if (movie.Actor == actor.name) {
                        newMovie["actorId"] = actor.id;
                    }
                })

                databaseGenre.forEach(genre => {
                    if (movie.Genre == genre.type) {
                        newMovie["genreId"] = genre.id;
                    }
                })
                newMovie = { ...movie, ...newMovie };
                delete newMovie.Director;
                delete newMovie.Actor;
                delete newMovie.Genre;
                newMovie.Year = newMovie.Year.toString();
                newMovieList.push(newMovie);

            })

            newMovieList.forEach(async (newMovie) => {
                if(newMovie.Gross_Earning_in_Mil == 'NA') {
                    delete newMovie.Gross_Earning_in_Mil
                }
                if(newMovie.Metascore == 'NA') {
                    delete newMovie.Metascore
                }
                await Movie
                    .create(newMovie)
                    .catch(err => {
                        logger.error(err);
                        res.status(500).json({
                            message: "internal server error",
                            err: err
                        });
                        res.end();
                    });
            })


            res.status(200).json({
                message: "movies data added successfully",
            })
            res.end();


        }
    })
})



module.exports = router;